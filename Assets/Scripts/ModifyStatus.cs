﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModifyStatus : MonoBehaviour
{
    public Character player;

    public Text strength, agility, vitality;
    public Text spText;

    public static int statusPoint = 30;

    void Start()
    {
        strength.text = player.str.ToString();
        agility.text = player.agi.ToString();
        vitality.text = player.vit.ToString();
        spText.text = "Status Point : " + statusPoint.ToString();
    }

    void Update()
    {
        strength.text = player.str.ToString();
        agility.text = player.agi.ToString();
        vitality.text = player.vit.ToString();
        spText.text = "Status Point : " + statusPoint.ToString();
    }

    public void ModifyButton(string modify)
    {
        switch (modify)
        {
            case "STR++":
                if (statusPoint >  0)
                {
                    player.str++;
                    statusPoint--;
                }
                break;
            case "STR--":
                if (player.str > 1)
                {
                    player.str--;
                    statusPoint++;
                }
                break;
            case "AGI++":
                if (statusPoint > 0)
                {
                    player.agi++;
                    statusPoint--;
                }
                break;
            case "AGI--":
                if (player.agi > 1)
                {
                    player.agi--;
                    statusPoint++;
                }
                break;
            case "VIT++":
                if (statusPoint > 0)
                {
                    player.vit++;
                    statusPoint--;
                }
                break;
            case "VIT--":
                if (player.vit > 1)
                {
                    player.vit--;
                    statusPoint++;
                }
                break;
            default:
                break;
        }
    }
}

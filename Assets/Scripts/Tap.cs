﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Tap : MonoBehaviour
{
    public GameObject tapText;

    public AudioSource tap;

    private float timeScale;


    void Update()
    {

        if (Input.touchCount > 0 || Input.GetKeyDown(KeyCode.E))
        {
            tap.Play();
            if (PlayerPrefs.GetInt("FIRSTTIME") == 0)
            {
                SceneManager.LoadScene("Cutscene");
                PlayerPrefs.SetInt("FIRSTTIME", 1);
            }
            else
                SceneManager.LoadScene("Home");

        }

        if (timeScale > 0)
            timeScale -= Time.deltaTime;
        else
        {
            tapText.SetActive(!tapText.activeSelf);
            timeScale = 0.5f;
        }
    }
}

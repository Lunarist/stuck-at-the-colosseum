﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : ScriptableObject
{
    public new string name;
    public int cooldown;
    public Sprite image;

    public abstract void Effect(Character player);
    public abstract void Deffect(Character player);
}

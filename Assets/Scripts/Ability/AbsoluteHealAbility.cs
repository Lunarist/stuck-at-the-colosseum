﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ability/AbsoluteHeal")]
public class AbsoluteHealAbility : Ability
{
    public int substract = 20;

    public override void Effect(Character player)
    {
        player.currHealth = player.health;
        player.currDefense -= ((substract / 100) * player.defense);
        Debug.Log("Using Absolute Heal Ability");
    }

    public override void Deffect(Character player)
    {

    }
}

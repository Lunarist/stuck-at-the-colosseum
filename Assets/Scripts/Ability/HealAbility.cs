﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ability/Heal")]
public class HealAbility : Ability
{
    public float add = 10;

    public override void Effect(Character player)
    {
        float addHealth = (add / 100) * player.health;
        Debug.Log("Health : " + addHealth.ToString());

        player.currHealth += addHealth;

        if (player.currHealth > player.health)
            player.currHealth = player.health;

        Debug.Log("Using Heal Ability");
    }

    public override void Deffect(Character player)
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ability/Guard")]
public class GuardAbility : Ability
{
    public float add = 5;

    public override void Effect(Character player)
    {
        player.currDefense += ((add / 100) * player.defense);
        Debug.Log("Using Guard Ability");
    }

    public override void Deffect(Character player)
    {
        player.currDefense = player.defense;
    }
}

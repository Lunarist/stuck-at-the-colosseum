﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Ability/Berserk")]
public class BerserkAbility : Ability
{
    public int add = 5;
    public int substract = 10;

    public override void Effect(Character player)
    {
        player.currAttack += ((add / 100) * player.attack);
        player.currHealth -= ((substract / 100) * player.health);
        Debug.Log("Using Berserker Ability");
    }

    public override void Deffect(Character player)
    {
        player.currAttack = player.attack;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UsingAbility : MonoBehaviour
{
    public Ability ability;

    public Text cooldownText;

    public GameObject cooldownIcon;

    public static int currCD;

    public static bool isUsed;

    void Start()
    {
        cooldownText.text = this.ability.cooldown.ToString();
        currCD = this.ability.cooldown;
    }

    void Update()
    {
        if (isUsed && currCD == 0)
        {
            cooldownIcon.SetActive(false);
            currCD = ability.cooldown;
            isUsed = false;
        }

        cooldownText.text = currCD.ToString();
    }

    public void AbilityUsed(Character player)
    {
        this.ability.Effect(player);
        isUsed = true;
        cooldownIcon.SetActive(true);
    }
}

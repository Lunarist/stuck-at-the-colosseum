﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnBase : MonoBehaviour
{
    public enum TurnBaseState { START, WAIT, PLAYERTURN, ENEMYTURN, WIN, LOSE, END};
    private TurnBaseState currentState;

    public GameObject playerTurnUI;
    public GameObject winUI, loseUI;

    public Character player;
    public Character[] enemy;

    public Animation playerAnim;
    public Animation enemyAnim;

    public Text playerLvl, enemyLvl;
    public Text turnText;
    public Image playerImage, enemyImage;
    public Slider playerHP, enemyHP;

    public static int turn;
    public static int level;
    public static bool isClear;

    private bool myTurn;
    private float timeScale = 1.5f;

    public AudioSource bgm, sfx;

    void Start()
    {
        bgm.volume = Settings.musicVolume;
        sfx.volume = Settings.soundsVolume;

        if (Level.selectLvl != 0)
        {
            level = Level.selectLvl - 1;
        }
        else
        {
            if (PlayerPrefs.GetInt("Level") == 0)
                level = 0;
            else
                level = PlayerPrefs.GetInt("Level");
        }

        currentState = TurnBaseState.START;
        turnText.text = "TURN : " + turn.ToString();
    }

    void Update()
    {
        switch (currentState)
        {
            case TurnBaseState.START:
                StartState();
                break;
            case TurnBaseState.WAIT:
                WaitState();
                break;
            case TurnBaseState.PLAYERTURN:
                PlayerTurnState();
                break;
            case TurnBaseState.ENEMYTURN:
                EnemyTurnState();
                break;
            case TurnBaseState.WIN:
                WinState();
                break;
            case TurnBaseState.LOSE:
                LoseState();
                break;
            case TurnBaseState.END:
                EndState();
                break;
        }
    }

    private void StartState()
    {
        turn = 0;

        // Instantiate Player
        playerImage.sprite = player.image;
        playerHP.value = 1;
        player.ConvertStatus();
        playerLvl.text = "Lvl. " + player.level.ToString();

        // Instantiate Enemy
        enemyImage.sprite = enemy[level].image;
        enemyHP.value = 1;
        enemy[level].ConvertStatus();
        enemyLvl.text = "Lvl. " + enemy[level].level.ToString();

        if (player.agi >= enemy[level].agi)
            currentState = TurnBaseState.PLAYERTURN;
        else
            currentState = TurnBaseState.ENEMYTURN;
    }

    private void  WaitState()
    {
        if (timeScale > 0)
        {
            timeScale -= Time.deltaTime;
        }
        else
        {
            timeScale = 1.5f;

            if (myTurn)
            {
                turn++;
                turnText.text = "TURN : " + turn.ToString();

                if (UsingAbility.isUsed)
                    UsingAbility.currCD--;

                currentState = TurnBaseState.PLAYERTURN;
            }
            else
            {
                currentState = TurnBaseState.ENEMYTURN;
            }
        }
    }

    private void PlayerTurnState()
    {
        playerTurnUI.SetActive(true);
    }

    private void EnemyTurnState()
    {
        enemyAnim.Play("EnemyAttack");
        playerAnim.Play("PlayerAttacked");

        if (Attack(player, enemy[level]) > 0)
        {
            player.currHealth -= Attack(enemy[level], player);
            Debug.Log(player.currHealth.ToString());
            playerHP.value = player.currHealth / player.health;
        }
            

        myTurn = true;

        if (player.currHealth > 0)
            currentState = TurnBaseState.WAIT;
        else
            currentState = TurnBaseState.LOSE;
    }

    private void WinState()
    {
        if (level <= enemy.Length)
            level++;
        else
            isClear = true;

        level++;
        PlayerPrefs.SetInt("Level", level);

        player.level++;
        ModifyStatus.statusPoint += 3;

        if (turn <= 3)
            Key.key += 3;
        else if (turn <= 6)
            Key.key += 2;
        else
            Key.key += 1;

        winUI.SetActive(true);
        currentState = TurnBaseState.END;
    }

    private void LoseState()
    {
        loseUI.SetActive(true);
        currentState = TurnBaseState.END;
    }

    private void EndState()
    {
        turn = 0;
        Level.selectLvl = 0;
    }

    private float Attack(Character attacker, Character defender)
    {
        float damage;

        if (attacker.currAttack > defender.currDefense)
            damage = attacker.currAttack - (100/ (100 + defender.currDefense));
        else
            damage = 1;

        return damage;
    }

    public void DoAttack()
    {
        sfx.Play();
        playerTurnUI.SetActive(false);

        playerAnim.Play("PlayerAttack");
        enemyAnim.Play("EnemyAttacked");

        if (Attack(player, enemy[level]) > 0)
        {
            enemy[level].currHealth -= Attack(player, enemy[level]);
            Debug.Log(enemy[level].currHealth.ToString());
            enemyHP.value -= Attack(player, enemy[level]) / enemy[level].health;
        }

        myTurn = false;

        if (enemy[level].currHealth > 0)
        {
            currentState = TurnBaseState.WAIT;
        }
        else
            currentState = TurnBaseState.WIN;
    }
}

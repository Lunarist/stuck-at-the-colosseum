﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character")]
public class Character : ScriptableObject
{
    public new string name;

    public int str;
    public int agi;
    public int vit;
    public int level;

    public float currAttack, attack;
    public float currSpeed, speed;
    public float currDefense, defense;
    public float currHealth, health;

    public Sprite image;

    public void ConvertStatus()
    {
        attack = str + (agi / 2);
        speed = agi;
        defense = vit;
        health = vit;

        currAttack = attack;
        currSpeed = speed;
        currDefense = defense;
        currHealth = health;
    }
}

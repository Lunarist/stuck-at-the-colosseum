﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour
{
    public GameObject leaderBoardUI;
    public Text leaderBoard;

    void Start()
    {
        leaderBoard.text = PlayerPrefs.GetInt("HIGHSCORE").ToString();
    }

    private void Update()
    {
        leaderBoard.text = PlayerPrefs.GetInt("HIGHSCORE").ToString();
    }

    public void ShowLeaderBoard()
    {
        leaderBoardUI.SetActive(!leaderBoardUI.activeSelf);
    }
}

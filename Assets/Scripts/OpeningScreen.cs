﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OpeningScreen : MonoBehaviour
{
    public Text tText;
    public Button bButton;
    //public AudioSource bgm;
    //public AudioSource sfx;

    private bool fading;

    private void Start()
    {
        fading = false;
        //bgm.Play();
        bButton.onClick.AddListener(MoveScene);
    }

    private void Update()
    {
        //bgm.loop = true;

        if (fading == false) 
        {
            tText.CrossFadeAlpha(0, 0.05f, false);
            fading = true;
        }
        else 
        {
            tText.CrossFadeAlpha(1, 0.05f, false);
            fading = false;
        }
    }

    private void MoveScene()
    {
        //sfx.Play();
        SceneManager.LoadScene("Home");
    }
}

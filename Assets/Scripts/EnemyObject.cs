﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "EnemyList", order = 1)]
public class EnemyObject : ScriptableObject
{
    public string enemyName = "Enemy Name";
    public int enemyAttack = 10;
    public int enemyDefense = 10;
    public int enemyHealth = 10;
    public int enemySpeed = 10;
    public Vector2 spawnPoint;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public Button onoffMusic, onoffSounds;

    public Sprite on, off;

    public AudioSource buttonEffect;

    public static int soundsVolume, musicVolume;

    void Start()
    {

        onoffMusic.GetComponent<Image>().sprite = on;
        onoffSounds.GetComponent<Image>().sprite = on;
        soundsVolume = 1;
        musicVolume = 1;
    }

    void Update()
    {
        buttonEffect.volume = soundsVolume;
    }

    public void OnOffMusic()
    {
        buttonEffect.Play();

        if (onoffMusic.image.sprite == on)
        {
            onoffMusic.image.sprite = off;
            musicVolume = 0;
        }
        else
        {
            onoffMusic.image.sprite = on;
            musicVolume = 1;
        }
    }

    public void OnOffSounds()
    {
        buttonEffect.Play();

        if (onoffSounds.image.sprite == on)
        {
            onoffSounds.image.sprite = off;
            soundsVolume = 0;
        }
        else
        {
            onoffSounds.image.sprite = on;
            soundsVolume = 1;
        }
    }
}

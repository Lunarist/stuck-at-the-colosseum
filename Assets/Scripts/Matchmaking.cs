﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Matchmaking : MonoBehaviour
{
    public enum TurnBaseState { START, WAIT, PLAYERTURN, ENEMYTURN, WIN, LOSE, END };
    private TurnBaseState currentState;

    public GameObject playerTurnUI;
    public GameObject winUI, loseUI, leaderBoardUI;

    public Character player;
    public Character[] enemy;

    public Animation playerAnim;
    public Animation enemyAnim;

    public Text playerLvl, enemyLvl;
    public Text turnText;
    public Image playerImage, enemyImage;
    public Slider playerHP, enemyHP;

    public static int turn;
    public static int level;
    public static bool isClear;

    private bool myTurn;
    private float timeScale = 1.5f;

    public AudioSource bgm, sfx;

    void Start()
    {
        bgm.volume = Settings.musicVolume;
        sfx.volume = Settings.soundsVolume;

        level = Random.Range(0, enemy.Length);

        turn = 0;
        turnText.text = "TURN : " + turn.ToString();
        currentState = TurnBaseState.START;
    }

    void Update()
    {
        switch (currentState)
        {
            case TurnBaseState.START:
                StartState();
                break;
            case TurnBaseState.WAIT:
                WaitState();
                break;
            case TurnBaseState.PLAYERTURN:
                PlayerTurnState();
                break;
            case TurnBaseState.ENEMYTURN:
                EnemyTurnState();
                break;
            case TurnBaseState.WIN:
                WinState();
                break;
            case TurnBaseState.LOSE:
                LoseState();
                break;
            case TurnBaseState.END:
                EndState();
                break;
        }
    }

    private void StartState()
    {
        // Instantiate Player
        playerImage.sprite = player.image;
        playerHP.value = 1;
        player.ConvertStatus();
        playerLvl.text = "Lvl. " + player.level.ToString();

        // Instantiate Enemy
        enemyImage.sprite = enemy[level].image;
        enemyHP.value = 1;
        MatchStatus();
        enemyLvl.text = "Lvl. " + enemy[level].level.ToString();

        if (player.agi >= enemy[level].agi)
            currentState = TurnBaseState.PLAYERTURN;
        else
            currentState = TurnBaseState.ENEMYTURN;
    }

    private void WaitState()
    {
        if (timeScale > 0)
        {
            timeScale -= Time.deltaTime;
        }
        else
        {
            timeScale = 1.5f;

            if (myTurn)
            {
                turn++;
                turnText.text = "TURN : " + turn.ToString();

                if (UsingAbility.isUsed)
                    UsingAbility.currCD--;

                currentState = TurnBaseState.PLAYERTURN;
            }
            else
            {
                currentState = TurnBaseState.ENEMYTURN;
            }
        }
    }

    private void PlayerTurnState()
    {
        playerTurnUI.SetActive(true);
    }

    private void EnemyTurnState()
    {
        enemyAnim.Play("EnemyAttack");
        playerAnim.Play("PlayerAttacked");

        if (Attack(player, enemy[level]) > 0)
        {
            player.currHealth -= Attack(enemy[level], player);
            Debug.Log(player.currHealth.ToString());
            playerHP.value = player.currHealth / player.health;
        }


        myTurn = true;

        if (player.currHealth > 0)
            currentState = TurnBaseState.WAIT;
        else
            currentState = TurnBaseState.LOSE;
    }

    private void WinState()
    {
        player.level++;
        ModifyStatus.statusPoint += 3;

        if (turn > PlayerPrefs.GetInt("HIGHSCORE"))
            PlayerPrefs.SetInt("HIGHSCORE", turn);

        winUI.SetActive(true);
        currentState = TurnBaseState.END;
    }

    private void LoseState()
    {
        loseUI.SetActive(true);
        currentState = TurnBaseState.END;
    }

    private void EndState()
    {
        turn = 0;
        Level.selectLvl = 0;
    }

    private float Attack(Character attacker, Character defender)
    {
        float damage;

        if (attacker.currAttack > defender.currDefense)
            damage = attacker.currAttack - (100 / (100 + defender.currDefense));
        else
            damage = 1;

        return damage;
    }

    public void DoAttack()
    {
        sfx.Play();
        playerTurnUI.SetActive(false);

        playerAnim.Play("PlayerAttack");
        enemyAnim.Play("EnemyAttacked");

        if (Attack(player, enemy[level]) > 0)
        {
            enemy[level].currHealth -= Attack(player, enemy[level]);
            Debug.Log(enemy[level].currHealth.ToString());
            enemyHP.value -= Attack(player, enemy[level]) / enemy[level].health;
        }

        myTurn = false;

        if (enemy[level].currHealth > 0)
        {
            currentState = TurnBaseState.WAIT;
        }
        else
            currentState = TurnBaseState.WIN;
    }

    private void MatchStatus()
    {
        int totalStatus = player.str + player.agi + player.vit;
        int random = Random.Range(1, totalStatus + 1);
        float random2, random3;

        if (totalStatus - random > 0)
            random2 = Random.Range(1, totalStatus - random + 1);
        else
            random2 = 0;

        if ((totalStatus - random) - random2 > 0)
            random3 = Random.Range(1, (totalStatus - random) - random2 + 1);
        else
            random3 = 0;

        enemy[level].currSpeed = random;
        enemy[level].currAttack = enemy[level].currSpeed + random2;
        enemy[level].currDefense = random3;
        enemy[level].currHealth = random3;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Home : MonoBehaviour
{
    public GameObject statusUI, skillUI, settingsUI, battleUI;
    public Button matchmaking, colosseum;

    public AudioSource buttonEffect, bgm;

    void Start()
    {
        buttonEffect.volume = Settings.soundsVolume;
        bgm.volume = Settings.musicVolume;

        bgm.Play();

        matchmaking.onClick.AddListener(() => { buttonEffect.Play();  SceneManager.LoadSceneAsync("Matchmaking"); });
        colosseum.onClick.AddListener(() =>
        {
            buttonEffect.Play();
            Key.key--;

            if (TurnBase.isClear)
                SceneManager.LoadScene("ColosseumLevel");
            else
                SceneManager.LoadScene("Colosseum");
        });

        if (PlayerPrefs.GetInt("INJAIL") == 0)
        {
            statusUI.SetActive(true);
            PlayerPrefs.SetInt("INJAIL", 1);
        }
    }

    private void Update()
    {
        buttonEffect.volume = Settings.soundsVolume;
        bgm.volume = Settings.musicVolume;
    }

    public void BattleUI()
    {
        buttonEffect.Play();
        battleUI.SetActive(!battleUI.activeSelf);
    }
    
    public void StatusUI()
    {
        buttonEffect.Play();
        statusUI.SetActive(!statusUI.activeSelf);
    }

    public void SkillUI()
    {
        buttonEffect.Play();
        skillUI.SetActive(!skillUI.activeSelf);
    }

    public void SettingsUI()
    {
        buttonEffect.Play();
        settingsUI.SetActive(!settingsUI.activeSelf);
    }

    public void CreditScene()
    {
        buttonEffect.Play();
        SceneManager.LoadScene("Credit");
    }
}

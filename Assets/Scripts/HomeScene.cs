﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeScene : MonoBehaviour
{
    public GameObject createNewUI;
    public GameObject currentUI;
    public Button createButton;

    private bool newPlayer;

    private void Start()
    {
        newPlayer = true;

        if (newPlayer)
            createNewUI.SetActive(true);
        else
            currentUI.SetActive(true);

        createButton.onClick.AddListener(CreateCharacter);
    }

    private void CreateCharacter()
    {

    }
}

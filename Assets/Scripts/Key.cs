﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Key : MonoBehaviour
{
    public static int key;
    public Text keyText;

    void Start()
    {
        if (PlayerPrefs.GetInt("FIRSTKEY") == 0)
        {
            key = 1;
        }

        keyText.text = "Key : " + key.ToString();
    }

    private void Update()
    {
        keyText.text = "Key : " + key.ToString();
    }

}

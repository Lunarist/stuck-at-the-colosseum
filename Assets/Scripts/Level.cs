﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    public static int selectLvl;

    public void SelectLevel(int level)
    {
        selectLvl = level;
        SceneManager.LoadScene("Colosseum");
    }
}
